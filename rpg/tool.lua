--[[

This file adds skills to the tools for the RPG mod

Axes: Timber
Right-click trees with an axe to chop down the entire tree.
Higher skill level grants faster cooldowns.

Pickaxes: Miner's Luck
Right-click stone to turn it into a random ore.
Higher skill level grants better ores and faster cooldowns.

Shovels: Bulldozer
Right-click dirt to dig a 3x3x3 area.

by Ryan Dang

]]--

local data = minetest.get_mod_storage()
rpg.mod_data = data

-- if the cooldown is finished, return 0 and reset the cooldown.
-- otherwise, return remaining time in cooldown
local function do_cooldown(player, skill, duration)
	local idx = "cd "..player:get_player_name().." "..skill
	local now = minetest.get_us_time()
	local cd_start = tonumber(data:get_string(idx)) or 0
	local cd = duration - ((now - cd_start)/1000000)

	-- failsafe: if cooldown is greater than an hour, assume integer overflow
	if cd <= 0 or cd <= 3600 then
		data:set_string(idx, tostring(now))
		return cd
	end

	return cd
end

function rpg.resetCooldown(player, skill)
	data:set_string("cd "..player:get_player_name().." "..skill, "0")
end

local function isplus(n)
	return n ~= nil and n > 0
end

-- returns the leaves that accompany the tree type
local function leaves(name)
	if name == "default:tree" then
		return "default:leaves"
	elseif name == "default:jungletree" then
		return "default:jungleleaves"
	elseif name == "default:pine_tree" then
		return "default:pine_needles"
	elseif name == "default:acacia_tree" then
		return "default:acacia_leaves"
	elseif name == "default:aspen_tree" then
		return "default:aspen_leaves"
	end
end

-- returns the tree that accompany the leaves
local function trees(name)
	if name == "default:leaves" then
		return "default:tree"
	elseif name == "default:jungleleaves" then
		return "default:jungletree"
	elseif name == "default:pine_needles" then
		return "default:pine_tree"
	elseif name == "default:acacia_leaves" then
		return "default:acacia_tree"
	elseif name == "default:aspen_leaves" then
		return "default:aspen_tree"
	end
end

-- recursive function to cut a tree upwards
-- returns a count of the number of pieces of wood cut and the leaves cut
local function cutWood_(pos, count, opt)
	local node = minetest.get_node(pos)
	local name = node.name
	local type = "leaves"

	if string.find(name, "tree") then
		type = "wood"
	end
	
	if count[type] >= opt.limit[type] then
		return count
	end
	
	count[type] = count[type] + 1

	minetest.node_dig(pos, node, opt.player)
	
	-- down, then up, then out.
	local pos_list = {
		{x = pos.x, y = pos.y - 1, z = pos.z},
		{x = pos.x, y = pos.y + 1, z = pos.z},
		{x = pos.x - 1, y = pos.y, z = pos.z},
		{x = pos.x + 1, y = pos.y, z = pos.z},
		{x = pos.x, y = pos.y, z = pos.z + 1},
		{x = pos.x, y = pos.y, z = pos.z - 1},
	}
		
	
	for _, new_pos in ipairs(pos_list) do
		local new_node = minetest.get_node(new_pos)
		local new_name = new_node.name
		if new_name == name or new_name == leaves(name) or new_name == trees(name) then
			count = cutWood_(new_pos, {wood = count.wood, leaves = count.leaves}, opt)
		end
	end
	
	return count
end

-- chop down a tree, starting from the top.
-- other mods may make logs gravity blocks
local function cutWood(pos, count, opt, name)
	local upNode = 		minetest.get_node({x = pos.x, y = pos.y + 1, z = pos.z})
	name = name or minetest.get_node(pos).name
	if (upNode.name == name) then
		return cutWood({x = pos.x, y = pos.y + 1, z = pos.z}, count, opt, name)
	end
	return cutWood_(pos, {wood = count.wood, leaves = count.leaves}, opt)
end

-- axe skill
-- chops down an entire tree
local function axeRightclick(itemStack, player, pointed_thing)
	if pointed_thing.type == "node" then
		local pos = pointed_thing.under
		local block = minetest.get_node(pos)
		if string.find(block.name, "tree") then

			local axeLvl = tonumber(player:get_attribute("axeLvl"))

			-- checks if axe level is eligible to cut down entire trees
			if tonumber(axeLvl) == 0 then
				minetest.chat_send_player(player:get_player_name(), "You must have at least 1 Axe level to use Timber.")
			else
				-- full length of cooldown
				local cooldown = math.ceil(120 / axeLvl + 1)
				if cooldown < 1 then
					cooldown = 1
				end
				-- how many seconds are left in the cooldown
				local cd_left = do_cooldown(player, "axe", cooldown)



				if cd_left <= 0 then
					local woodName = block.name
					local limit = 5 + axeLvl
					local count = cutWood(pos, {wood = 0, leaves = 0}, 
						{ player = player , limit = { wood = limit, leaves = limit * 4} })


					minetest.chat_send_player(player:get_player_name(), "Timber! " .. count.wood .. " pieces of wood cut. " .. count.leaves .. " leaves cut.")

					-- adds exp
					rpg.addXP(player, "axe", count.wood)
				else
					minetest.chat_send_player(player:get_player_name(), "Your Timber skill is on cooldown: " .. math.ceil(cd_left) .. " seconds left")
				end
			end
		else
			minetest.chat_send_player(player:get_player_name(), "You must rightclick on a tree to use Timber.")
		end

	end
end

local function gaussian (mean, variance)
    return  math.sqrt(-2 * variance * math.log(math.random())) *
            math.cos(2 * math.pi * math.random()) + mean
end

local ore_values = {}

local function addOre(name, value)
	ore_values["default:stone_with_"..name] = value
	if name == "iron" then
		name = "steel"
	end
	ore_values["default:"..name.."block"] = value * 9
end

addOre("coal", 1)
addOre("iron", 3)
addOre("tin", 4)
addOre("copper", 5)
addOre("gold", 10)
addOre("mese", 15)
addOre("diamond", 20)

local function getOreOfValue(val)
	--minetest.chat_send_all("actual ore val: "..val)
	local r = {"default:stone_with_coal"}
	local u = 0
	for n, v in pairs(ore_values) do
		if v <= val and v >= u then
			table.insert(r, n)
			u = v
		end
	end
	return r[math.random(1, #r)]
end

local function getRandomOre(val)
	--minetest.chat_send_all("avg ore val: "..val)
	return getOreOfValue(gaussian(val, 16))
end

-- mining skill
-- turns stone into a random ore
local function pickaxeRightClick(itemStack, player, pointed_thing)
	if pointed_thing.type == "node" then
		local pos = pointed_thing.under
		local block = minetest.get_node(pos)
		if block.name == "default:stone" then
			local miningLvl = tonumber(player:get_attribute("miningLvl"))

			if tonumber(miningLvl) == 0 then
				minetest.chat_send_player(player:get_player_name(), "You must have at least 1 Mining level to use Miner's Luck.")
			else
				local cooldown = math.floor(120 / miningLvl + 1)
				if cooldown < 0 then
					cooldown = 0
				end
				local cd_left = do_cooldown(player, "mining", cooldown)

				if cd_left <= 0 then
					-- +3 -3 range, reaches maximum after 12 seconds
					local spamModifier = 3 - ((6 - math.min(-cd_left, 12))/2)
					local randomNode = getRandomOre(9 + (miningLvl/10) - 21/miningLvl + spamModifier)
					if string.find(randomNode, "block") then
						minetest.chat_send_player(player:get_player_name(), "Miner's Luck! Your stone turned into a block!")
					else
						minetest.chat_send_player(player:get_player_name(), "Miner's Luck! Your stone turned into ore!")
					end

					minetest.set_node(pos, {name = randomNode})

				else
					minetest.chat_send_player(player:get_player_name(), "Your Miner's Luck skill is on cooldown: " .. math.ceil(cd_left)  .. " seconds left")
				end
			end
		else
			minetest.chat_send_player(player:get_player_name(), "You must rightclick on stone to use Miner's Luck.")
		end
	end
end


local function diggable(name)
	return isplus(minetest.get_item_group(name, "crumbly"))
end

-- digs a 3 x 3 x 3 area
local function bulldozer(pos, player)
	local count = 0
	for x=1,3 do
		for z=1,3 do
			for y=1,3 do
				local pos2 = {x = pos.x - x + 2, y = pos.y - y + 2, z = pos.z - z + 2}
				local node = minetest.get_node(pos2)

				if diggable(node.name) then
					minetest.node_dig(pos2, node, player)
					count = count + 1
				end
			end
		end
	end
	return count
end

-- digging skill
-- digs a large area
local function shovelRightClick(itemStack, player, pointed_thing)
	if pointed_thing.type == "node" then
		local pos = pointed_thing.under
		local block = minetest.get_node(pos)
		if diggable(block.name) then
			local diggingLvl = player:get_attribute("diggingLvl")

			-- checks if digging level is eligible use skill
			if tonumber(diggingLvl) == 0 then
				minetest.chat_send_player(player:get_player_name(), "You must have at least 1 Digging level to use Bulldozer.")
			else
				local cooldown = math.floor(20 - diggingLvl)
				if cooldown < 0 then
					cooldown = 0
				end

				local cd_left = do_cooldown(player, "digging", cooldown)

				if cd_left <= 0 then
					local dirtDug = bulldozer(pos, player)

					minetest.chat_send_player(player:get_player_name(), "Bulldozer! " .. dirtDug .. " pieces of dirt dug up.")
					rpg.addXP(player, "digging", dirtDug)
				else
					minetest.chat_send_player(player:get_player_name(), "Your Bulldozer skill is on cooldown: " .. math.ceil(cd_left) .. " seconds left")
				end
			end
		else
			minetest.chat_send_player(player:get_player_name(), "You must rightclick on dirt to use Bulldozer.")
		end
	end
end




-- overrides

for name, item in pairs(minetest.registered_items) do
	if isplus(item.groups.axe) then
		minetest.override_item(name, { on_place = axeRightclick })
	elseif isplus(item.groups.pickaxe) then
		minetest.override_item(name, { on_place = pickaxeRightClick })
	elseif isplus(item.groups.shovel) then
		minetest.override_item(name, { on_place = shovelRightClick })
	end
end

rpg.diggable = diggable
