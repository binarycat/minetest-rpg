-- per-player settings
local data = rpg.mod_data

assert(data ~= nil)

local settings = {
	xp_notify = "threshold",
}

-- rpg functions
function rpg.getSetting(playername, settingname)
	local t = settings[settingname]
	local idx = "s "..playername.." "..settingname
	
	if t == "threshold" then
		return data:get_int(idx)
	else
		return nil, "unknown setting"
	end
end


function rpg.setSetting(playername, settingname, value)
	local t = settings[settingname]
	local idx = "s "..playername.." "..settingname
	
	if t == "threshold" then
		local v = 0
		if value == "off" then
			v = 9999999
		else
			v = tonumber(value)
		end
		data:set_int(idx, v)
	else
		return "unknown setting " .. settingname
	end
end